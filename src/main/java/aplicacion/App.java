package aplicacion;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;

/**
 * @author sergio
 */
public class App {
    static Connection con;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            con = ConexionBD.getConexion();
            if (con != null) {
                verMetadatos();
                //verDatosTabla();

                // Ejercicio1: método que recorra todas las tablas de la bbdd (excepto
                // lineas_factura) y, de cada una,
                // que muestre toda la información (metadatos y registros) tal y como lo hace el
                // método verDatosTabla
                // Sólo mostrar los 10 primeros registro de cada tabla.
                // De la tabla facturas se mostrará también el total de factura.
//				ejercicio1();

                // Ejercicio2: OPCIONAL
                // Lo mismo que el ejercicio1 pero las claves ajenas, en lugar de mostrar el
                // identificador, deben
                // mostrar los datos a donde apuntan: clientes.*, vendedores.* y grupos.*
                // Ejemplo: select a.*, g.* from articulos a inner join grupos g on a.grupo = g.id
                // Echa un vistazo al método de DatabaseMetaData: getImportedKeys()
//				ejercicio2();
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                ConexionBD.cerrar();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }

    }

    private static void verMetadatos() {
        try {
            DatabaseMetaData dbmd = con.getMetaData();
            // Informacion del producto de la base de datos
            infoProducto(dbmd);
            // Informacion del driver JDBC
            infoVersiones(dbmd);
            // Informacion sobre las funciones de la base de datos
            infoFunciones(dbmd);
            // Tablas existentes
            infoTablas(dbmd);
            // Procedimientos existentes
            infoProcedimientos(dbmd);
            // Bases de datos disponibles
            infoBD(dbmd);
        } catch (SQLException se) {
            System.err.println(se);
        }
    }

    public static void infoProducto(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Informacion sobre el SGBD:");
        String producto = dbmd.getDatabaseProductName();
        String version = dbmd.getDatabaseProductVersion();
        boolean soportaSQL = dbmd.supportsANSI92EntryLevelSQL();
        boolean soportaConvert = dbmd.supportsConvert();
        boolean usaFich = dbmd.usesLocalFiles();
        boolean soportaGRBY = dbmd.supportsGroupBy();
        boolean soportaMinSQL = dbmd.supportsMinimumSQLGrammar();
        boolean soportaTransac = dbmd.supportsTransactions();
        boolean soportaBatch = dbmd.supportsBatchUpdates();
        boolean soportaRsSensible = dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
        boolean soportaRsNoSensible = dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
        boolean soportaRsForward = dbmd.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY);

        String nombre = dbmd.getUserName();
        String url = dbmd.getURL();
        System.out.println(" Producto: " + producto + " " + version);
        System.out.println(" Soporta el SQL ANSI92: " + soportaSQL);
        System.out.println(" Soporta la funcion CONVERT entre tipos SQL: " + soportaConvert);
        System.out.println(" Almacena las tablas en ficheros locales: " + usaFich);
        System.out.println(" Nombre del usuario conectado: " + nombre);
        System.out.println(" URL de la Base de Datos: " + url);
        System.out.println(" Soporta GROUP BY: " + soportaGRBY);
        System.out.println(" Soporta la minima gramatica SQL: " + soportaMinSQL);
        System.out.println(" Soporta transacciones: " + soportaTransac);
        System.out.println(" Soporta ejecución en batch: " + soportaBatch);
        System.out.println(" Soporta resultset sensible: " + soportaRsSensible);
        System.out.println(" Soporta resultset no sensible: " + soportaRsNoSensible);
        System.out.println(" Soporta resultset unidireccional: " + soportaRsForward);

        System.out.println(" ...");
        System.out.println();
    }

    public static void infoVersiones(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Informacion sobre el driver:");
        String driver = dbmd.getDriverName();
        int jdbcMajorVersion = dbmd.getJDBCMajorVersion();
        int jdbcMinorVersion = dbmd.getJDBCMinorVersion();
        String driverVersion = dbmd.getDriverVersion();
        int verMayor = dbmd.getDriverMajorVersion();
        int verMenor = dbmd.getDriverMinorVersion();
        System.out.println("--------------------------- ");
        System.out.println(" JDBC Version: " + jdbcMajorVersion + "." + jdbcMinorVersion);
        System.out.println(" Driver: " + driver + " " + driverVersion);
        System.out.println(" Version superior del driver: " + verMayor);
        System.out.println(" Version inferior del driver: " + verMenor);
        System.out.println();
    }

    public static void infoFunciones(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Funciones del DBMS:");
        String funcionesCadenas = dbmd.getStringFunctions();
        String funcionesSistema = dbmd.getSystemFunctions();
        String funcionesTiempo = dbmd.getTimeDateFunctions();
        String funcionesNumericas = dbmd.getNumericFunctions();
        System.out.println(" Funciones de Cadenas: " + funcionesCadenas);
        System.out.println(" Funciones Numericas: " + funcionesNumericas);
        System.out.println(" Funciones del Sistema: " + funcionesSistema);
        System.out.println(" Funciones de Fecha y Hora: " + funcionesTiempo);
        System.out.println();

    }

    public static void infoTablas(DatabaseMetaData dbmd) throws SQLException {
        ResultSet fk;
        System.out.println(">>>Tablas existentes:");
        String patron = "%"; // listamos todas las tablas
        String tipos[] = new String[2];
        tipos[0] = "TABLE"; // tablas de usuario
        tipos[1] = "VIEW"; // vistas
//        tipos[2] = "SYSTEM TABLE";  //tablas del sistema
        ResultSet tablas = dbmd.getTables(null, null, patron, tipos);
        while (tablas.next()) {
            // Por cada tabla obtenemos su nombre y tipo
            System.out.println("Catálogo: " + tablas.getString("TABLE_CAT") + " - Esquema: "
                    + tablas.getString("TABLE_SCHEM") + " - Tipo: " + tablas.getString("TABLE_TYPE") + " - Nombre: "
                    + tablas.getString("TABLE_NAME"));

            fk = dbmd.getImportedKeys(con.getCatalog(), null, tablas.getString("TABLE_NAME"));

            if (!tablas.getString("TABLE_NAME").contains("lineas_factura")) {
                String query = "SELECT * FROM " + tablas.getString("TABLE_NAME") + " limit 10";
                if (tablas.getString("TABLE_NAME").contains("facturas")) {
                    query = "SELECT * , SUM(lineas_factura.cantidad * lineas_factura.importe) AS total FROM facturas INNER JOIN lineas_factura  ON facturas.id = lineas_factura.factura group by(facturas.id) limit 10;";
                    ejercicio1(tablas.getString("TABLE_NAME"), query);
                    query = "SELECT " + tablas.getString("TABLE_NAME") + ".*, SUM(lineas_factura.importe) as total";
                    while (fk.next()) {

                        query = query + ", " + fk.getString("PKTABLE_NAME") + ".*";

                        ejercicio2(query);
                    }
                } else {
                    while (fk.next()) {
                        query = query + ", " + fk.getString("PKTABLE_NAME") + ".*";
                        ejercicio2(query);
                    }
                    ejercicio1(tablas.getString("TABLE_NAME"), query);
                }

            }


        }
        System.out.println();

    }

    public static void infoProcedimientos(DatabaseMetaData dbmd) throws SQLException {
        if (dbmd.supportsStoredProcedures()) {
            System.out.println(">>>Procedimientos/Funciones almacenadas:");
            String patron = "%";
            ResultSet procedimientos = dbmd.getProcedures(null, "empresa_ad", patron);
            while (procedimientos.next()) {
                System.out.println("Procedimiento " + procedimientos.getString("PROCEDURE_NAME") + " - Tipo: "
                        + procedimientos.getString("PROCEDURE_TYPE"));
            }
            ResultSet funciones = dbmd.getFunctions(null, "empresa_ad", patron);
            while (funciones.next()) {
                System.out.println("Función " + funciones.getString("FUNCTION_NAME") + " - Tipo: "
                        + funciones.getString("FUNCTION_TYPE"));
            }
        } else {
            System.out.println(">>>El DBMS no soporta procedimientos almacenados");
        }
        System.out.println();
    }

    private static void infoBD(DatabaseMetaData dbmd) throws SQLException {
        System.out.println(">>>Bases de datos disponibles:");
        ResultSet rs = dbmd.getCatalogs();
        while (rs.next()) {
            System.out.println(rs.getString("TABLE_CAT"));
        }
        rs = dbmd.getSchemas();
        while (rs.next()) {
            System.out.println(rs.getString("TABLE_SCHEM"));
        }
    }

    private static void verDatosTabla() {
        int numColumnas;
        ResultSetMetaData rsmd;

        System.out.println("TABLA: articulos");
        try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery("SELECT * FROM articulos")) {

            // Obtiene metadatos de la consulta
            rsmd = rs.getMetaData();

            // Muestra total de campos
            numColumnas = rsmd.getColumnCount();
            System.out.println("Num. columnas: " + numColumnas);

            // Muestra información detallada de los campos
            for (int columna = 1; columna <= numColumnas; columna++) {
                System.out.println("Columna " + columna + " - Nombre: " + rsmd.getColumnName(columna)
                        + " - Tipo datos: " + rsmd.getColumnTypeName(columna) + "(" + rsmd.getColumnDisplaySize(columna)
                        + ")" + " Tam: " + rsmd.getPrecision(columna) + " " + rsmd.getScale(columna));

            }

            // Muestra registros formateados
            // Cabecera
            int[] tamanyos = new int[numColumnas + 1];
            tamanyos[0] = 0;
            for (int columna = 1; columna <= numColumnas; columna++) {
                tamanyos[columna] = rsmd.getColumnDisplaySize(columna);
                String nombreColumna = rsmd.getColumnName(columna);
                int tamDisplay = rsmd.getColumnDisplaySize(columna);
                if (nombreColumna.length() > tamDisplay) {
                    nombreColumna = nombreColumna.substring(0, tamDisplay);
                }
                System.out.print(nombreColumna);
                repiteCaracter(tamanyos[columna] - nombreColumna.length(), ' ');
                System.out.print("\t");
            }
            System.out.println();
            for (int columna = 1; columna <= numColumnas; columna++) {
                repiteCaracter(tamanyos[columna], '=');
                System.out.print("\t");
            }
            System.out.println("\n");
            // Registros
            Object dato;
            while (rs.next()) {
                for (int columna = 1; columna <= numColumnas; columna++) {
                    dato = rs.getObject(columna) != null ? rs.getObject(columna) : "";
                    System.out.print(dato);
                    repiteCaracter(tamanyos[columna] - dato.toString().length(), ' ');
                    System.out.print("\t");
                }
                System.out.println();
            }

        } catch (SQLException ex) {
            System.err.println("Error" + ex.getMessage());
        }

    }

    private static void ejercicio1(String nomTabla, String query) {
        //"SELECT * FROM"+ nomTabla  +"limit 10"
        int numColumnas;
        ResultSetMetaData rsmd;

        System.out.println("TABLA: " + nomTabla);
        try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(query)) {

            // Obtiene metadatos de la consulta
            rsmd = rs.getMetaData();

            // Muestra total de campos
            numColumnas = rsmd.getColumnCount();
            System.out.println("Num. columnas: " + numColumnas);

            // Muestra información detallada de los campos
            for (int columna = 1; columna <= numColumnas; columna++) {
                System.out.println("Columna " + columna + " - Nombre: " + rsmd.getColumnName(columna)
                        + " - Tipo datos: " + rsmd.getColumnTypeName(columna) + "(" + rsmd.getColumnDisplaySize(columna)
                        + ")" + " Tam: " + rsmd.getPrecision(columna) + " " + rsmd.getScale(columna));
            }

            // Muestra registros formateados
            // Cabecera
            int[] tamanyos = new int[numColumnas + 1];
            tamanyos[0] = 0;
            for (int columna = 1; columna <= numColumnas; columna++) {
                tamanyos[columna] = rsmd.getColumnDisplaySize(columna);
                String nombreColumna = rsmd.getColumnName(columna);
                int tamDisplay = rsmd.getColumnDisplaySize(columna);
                if (nombreColumna.length() > tamDisplay) {
                    nombreColumna = nombreColumna.substring(0, tamDisplay);
                }
                System.out.print(nombreColumna);
                repiteCaracter(tamanyos[columna] - nombreColumna.length(), ' ');
                System.out.print("\t");
            }
            System.out.println();
            for (int columna = 1; columna <= numColumnas; columna++) {
                repiteCaracter(tamanyos[columna], '=');
                System.out.print("\t");
            }
            System.out.println("\n");
            // Registros
            Object dato;
            while (rs.next()) {
                for (int columna = 1; columna <= numColumnas; columna++) {
                    dato = rs.getObject(columna) != null ? rs.getObject(columna) : "";
                    System.out.print(dato);
                    repiteCaracter(tamanyos[columna] - dato.toString().length(), ' ');
                    System.out.print("\t");
                }
                System.out.println();
            }

        } catch (SQLException ex) {
            System.err.println("Error" + ex.getMessage());
        }

    }

    private static void ejercicio2(String query) {
        int numColumnas;
        ResultSetMetaData rsmd;

        System.out.println("TABLA");
        try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(query)) {

            // Obtiene metadatos de la consulta
            rsmd = rs.getMetaData();

            // Muestra total de campos
            numColumnas = rsmd.getColumnCount();
            System.out.println("Num. columnas: " + numColumnas);

            // Muestra información detallada de los campos
            for (int columna = 1; columna <= numColumnas; columna++) {
                System.out.println("Columna " + columna + " - Nombre: " + rsmd.getColumnName(columna)
                        + " - Tipo datos: " + rsmd.getColumnTypeName(columna) + "(" + rsmd.getColumnDisplaySize(columna)
                        + ")" + " Tam: " + rsmd.getPrecision(columna) + " " + rsmd.getScale(columna));
            }

            // Muestra registros formateados
            // Cabecera
            int[] tamanyos = new int[numColumnas + 1];
            tamanyos[0] = 0;
            for (int columna = 1; columna <= numColumnas; columna++) {
                tamanyos[columna] = rsmd.getColumnDisplaySize(columna);
                String nombreColumna = rsmd.getColumnName(columna);
                int tamDisplay = rsmd.getColumnDisplaySize(columna);
                if (nombreColumna.length() > tamDisplay) {
                    nombreColumna = nombreColumna.substring(0, tamDisplay);
                }
                System.out.print(nombreColumna);
                repiteCaracter(tamanyos[columna] - nombreColumna.length(), ' ');
                System.out.print("\t");
            }
            System.out.println();
            for (int columna = 1; columna <= numColumnas; columna++) {
                repiteCaracter(tamanyos[columna], '=');
                System.out.print("\t");
            }
            System.out.println("\n");
            // Registros
            Object dato;
            while (rs.next()) {
                for (int columna = 1; columna <= numColumnas; columna++) {
                    dato = rs.getObject(columna) != null ? rs.getObject(columna) : "";
                    System.out.print(dato);
                    repiteCaracter(tamanyos[columna] - dato.toString().length(), ' ');
                    System.out.print("\t");
                }
                System.out.println();
            }

        } catch (SQLException ex) {
            System.err.println("Error" + ex.getMessage());
        }
    }

    private static void repiteCaracter(int numVeces, char c) {
        System.out.print(new String(new char[numVeces]).replace('\0', c));

    }

}
